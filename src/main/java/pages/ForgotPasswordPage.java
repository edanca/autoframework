package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ForgotPasswordPage extends BasePage {

    private WebDriver driver;

    @FindBy(css = "div.forgotPassword > span.linkLook")
    private WebElement spanRegistrarse_elem;

    public ForgotPasswordPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, this.pageExplicitWaitFromFile()), this);
    }

    public void clickRegistrarse() {
        spanRegistrarse_elem.click();
    }
}
