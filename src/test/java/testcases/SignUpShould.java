package testcases;

import base.TestBase;
import operations.Actions;
import helpers.ReadProperties;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.SignUpPage;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class SignUpShould extends TestBase {

    //private WebDriver driver;
    private String url;
    private ArrayList<String> tabs;

    public SignUpShould() throws MalformedURLException {
    }

    @BeforeClass
    @Parameters({"browser","port"})
    public void setUpClass(String browser, String port) throws MalformedURLException {
        this.url = baseURI;
        this.setBrowser(browser, port);
        System.out.println("============ START TESTING ============");
    }

    @AfterClass
    public void tearDownClass() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.out.println("Driver already closed");
        }
        System.out.println("============= END TESTING =============");
    }

    @BeforeMethod
    public void setUp() {
        driver.navigate().to(this.url);
        driver.manage().window().maximize();
        int timeout = Integer.parseInt(ReadProperties.readPropertiesFile("pageImplicitWait"));
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
        tabs = getBrowserTabs();
        if (tabs.size() != 1) {
            driver.switchTo().window(tabs.get(tabs.size()-1));
        } else {
            driver.switchTo().window(tabs.get(0));
        }
    }

    /****************************************************
     *
     * TEST CASES
     *
     ****************************************************/

    @Test(description = "Perform a success sign up" ,
            groups = {"success", "signup_success"})
    //@Parameters({"success_email"})
    public void success() {
        //String email = "abcdefg12345678907@yopmail.com";
        this.getTestDataFromFile();
        String email = this.success_email;
        Actions actions = new Actions(driver);
        actions.signUp(email);

        boolean result = SignUpPage.validateSignUpSuccess(driver);

//        setUpdateTestDataToFile();

        Assert.assertTrue(result);
    }

    @Test(description = "Try to perform a sign up but fail due the email it was alreay registered",
            groups = {"exceptions", "signup_exceptions"})
    @Parameters({"failByAlreadyRegisteredEmail_email"})
    public void failByAlreadyRegisteredEmail(String failByAlreadyRegisteredEmail_email) {
        //String email = "abc@mail.com";
        Actions actions = new Actions(driver);
        actions.signUp(failByAlreadyRegisteredEmail_email);

        boolean result = SignUpPage.validateEmailAlreadyRegistered(driver);

        Assert.assertTrue(result);
    }

    @Test(description = "Try to perform a sign up but fail due the email entered is not well formated",
            groups = {"exceptions", "signup_exceptions"})
    @Parameters({"failByIncorrectEmail_email"})
    public void failByIncorrectEmail(String failByIncorrectEmail_email) {
        //String email = "*******";
        Actions actions = new Actions(driver);
        actions.signUp(failByIncorrectEmail_email);

        boolean result = SignUpPage.validateInvalidEmail(driver);

        Assert.assertTrue(result);
    }

    @Test(description = "Try to perform a sign up but fail due the no email was entered",
            groups = {"exceptions", "signup_exceptions"})
    @Parameters({"failByEmptyEmail_name"})
    public void failByEmptyEmail(String failByEmptyEmail_name) {
        //String email = "";
        Actions actions = new Actions(driver);
        actions.signUp(failByEmptyEmail_name);

        boolean result = SignUpPage.validateEmptyEmail(driver);

        Assert.assertTrue(result);
    }
}
