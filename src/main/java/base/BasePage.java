package base;

import helpers.ReadProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    public static String browser;
    protected WebDriver driver;
    protected WebDriverWait wait;

    public BasePage() {

    }

    public void waiting() {

    }

    public int pageExplicitWaitFromFile() {
        String pageExplicitWait = "pageExplicitWait";
        int timeout;
        timeout = Integer.parseInt(ReadProperties.readPropertiesFile(pageExplicitWait));
        return timeout;
    }
}
