package helpers;

public class Log {

    public static void printLog(String validationName, String expected, String received) {
        System.out.println("-------------------");
        System.out.println("VALIDATION NAME: " + validationName);
        System.out.println("EXPECTED: " + expected);
        System.out.println("RECEIVED: " + received);
        System.out.println("-------------------");
    }
}
