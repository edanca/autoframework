package pages;

import base.BasePage;
import helpers.Log;
import helpers.Screenshoter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.Collator;

public class SignUpPage extends BasePage {

    private WebDriver driver;

    @FindBy(xpath = "//input[@type='email']")
    private WebElement fieldEmail_elem;
    @FindBy(name = "userSignupButton")
    private WebElement btnRegistrarse;
    // --- Success Sign Up
    @FindBy(css = "section.adb-local_alert__success")
    private WebElement successSignUpAlert;
    @FindBy(css = "section.adb-local_alert__success > div > h3")
    private WebElement successSignUpAlertTitle;
    @FindBy(css = "section.adb-local_alert__success > div > p:nth-child(2)")
    private WebElement successSignUpAlert1stParagraph;
    @FindBy(css = "section.adb-local_alert__success > div > p:nth-child(3)")
    private WebElement successSignUpAlert2ndParagraph;
    //---

    /****************************************************
     *
     * HANDLE PAGE
     *
     ****************************************************/

    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, this.pageExplicitWaitFromFile()), this);
    }

    public void submitEmail(String email) {
        fieldEmail_elem.sendKeys(email);
        btnRegistrarse.click();
    }

    /****************************************************
     *
     * PAGE VALIDATIONS
     *
     ****************************************************/


    public static boolean validateSignUpSuccess(WebDriver driver) {
        // TODO: change when logs in file are available
        SignUpPage sp = new SignUpPage(driver);
        if (!sp.successSignUpAlert.isDisplayed()) {
            Screenshoter.takeScreenShot(driver, "ERROR_SIGNUPHOULD_success_SignUp_Alert_is_not_Displayed");
            return false;
        }
        if(!sp.validateSigUpSuccessTitle()) {
            System.out.println("Warning: SignUp processs validate that succes title is erroneous.");
            Screenshoter.takeScreenShot(driver, "ERROR_SIGNUPHOULD_validateSigUpSuccessTitle");
            return false;
        }
        if(!sp.validateSignUpSuccess1stParagraph() && !sp.validateSignUpSuccess2ndParagraph()) {
            System.out.println("Warning: SignUp process validate that success paragraph is erronoues.");
            Screenshoter.takeScreenShot(driver, "ERROR_SIGNUPHOULD_validateSignUpSuccess1stParagraph");
            return false;
        }
        return true;
    }

    public static boolean validateEmailAlreadyRegistered(WebDriver driver) {
        String errorText = "Esta dirección de correo electrónico ya está registrada en nuestro sistema. Regístrese con otra dirección de correo electrónico.";
        String css_selector_emailAlreadyRegistred = "div.adb-local_alert__error";
        By emailAlrady = By.cssSelector(css_selector_emailAlreadyRegistred);

        WebElement emailAlreadyRegistered_elem = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(emailAlrady));

        final Collator instance = Collator.getInstance();
        instance.setStrength(Collator.PRIMARY);
        instance.compare(emailAlreadyRegistered_elem.getText(),errorText);

        if(!emailAlreadyRegistered_elem.isDisplayed()) {
            String received = Boolean.toString(emailAlreadyRegistered_elem.isDisplayed());
            Log.printLog("emailAlreadyRegistered_elem.isDisplayed() FAILED.", "emailAlreadyRegistered_elem to be displayed", received);
            Screenshoter.takeScreenShot(driver, "ERROR_SIGNUPHOULD_emailAlreadyRegistered_elem_notDisplayed");
            return false;
        }
        /*if (instance.compare(emailAlreadyRegistered_elem.getText(),errorText) != 0) {
            Log.printLog("instance.compare(emailAlreadyRegistered_elem.getText(),errorText) != 0 FAILED.", errorText, emailAlreadyRegistered_elem.getText());
            return false;
        }*/
        return true;
    }

    public static boolean validateInvalidEmail(WebDriver driver) {
        String xpath_invalid_email = "//input[@aria-invalid='true']";
        By emailFieldWithError = By.xpath(xpath_invalid_email);

        String css_selector_invalid_email = "input.adb-text__full_width.user-error";
        By emailFieldWithErrorCSS = By.cssSelector(css_selector_invalid_email);

        WebElement failEmail_elem = (new WebDriverWait(driver, 15))
                .until(ExpectedConditions.presenceOfElementLocated(emailFieldWithError));

        if (!failEmail_elem.isEnabled() && !failEmail_elem.isDisplayed()) {
            Screenshoter.takeScreenShot(driver, "ERROR_SIGNUPHOULD_failEmail_not_displayed_or_not_enabled");
            return false;
        }
        return true;
    }

    public static boolean validateEmptyEmail(WebDriver driver) {
        String css_selector_empty_email = "//input[@aria-invalid='true' and @class='adb-text__full_width user-error']";
        By emailFieldAsEmpty = By.xpath(css_selector_empty_email);

        WebElement emptyEmail_elem = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(emailFieldAsEmpty));

        if (!emptyEmail_elem.isEnabled() && !emptyEmail_elem.isDisplayed()) {
            Log.printLog("!emptyEmail_elem.isEnabled or !emptyEmail_elem.isDisplayed FAILED", "emptyEmail_elem displayed or enalbed", "FALSE");
            Screenshoter.takeScreenShot(driver, "ERROR_SIGNUPHOULD_emptyEmail_elem_is_not_displayed_or_not enabled");
            return false;
        }
        return true;
    }

    /****************************************************
     *
     *  PRIVATE FUNCTIONS
     *
     ***************************************************/

    private boolean validateSigUpSuccessTitle() {
        String successTitle_es = "Gracias por su interés.";
        String successTitle_en = "Thanks for your interest.";
        if (successSignUpAlertTitle.getText().equals(successTitle_es) ||
            successSignUpAlertTitle.getText().equals(successTitle_en)) {
            return true;
        }
        return false;
    }

    private boolean validateSignUpSuccess1stParagraph() {
        String success1stParagraph_es = "Gracias por enviar su solicitud para registrarse en AppDirect.";
        String success1stParagraph_en = "Thank you for submitting your request to sign up for AppDirect.";
        if (successSignUpAlert1stParagraph.getText().equals(success1stParagraph_es) ||
            successSignUpAlert1stParagraph.getText().equals(success1stParagraph_en)) {
            return true;
        }
        return false;
    }

    private boolean validateSignUpSuccess2ndParagraph() {
        String success2ndParagraph_es = "Estamos revisando su solicitud. Recibirá un correo electrónico para activar su cuenta cuando la solicitud se haya aprobado.";
        String success2ndParagraph_en = "We are reviewing your request. You will receive an email to activate your account once your request has been approved.";
        if (successSignUpAlert2ndParagraph.getText().equals(success2ndParagraph_es) ||
            successSignUpAlert2ndParagraph.getText().equals(success2ndParagraph_en)) {
            return true;
        }
        return false;
    }
}
