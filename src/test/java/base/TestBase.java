package base;

import helpers.ReadProperties;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Parameters;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;

public class TestBase {

    protected WebDriver driver;
    protected String baseURI;
    private String browserName;

    protected String success_email;
    protected String failByAlreadyRegisteredEmail_email;
    protected String failByIncorrectEmail_email;
    protected String faildByEmptyEmail_name;

    public TestBase() throws MalformedURLException {
        this.baseURI = "https://www.appdirect.com/";
        this.getBrowserName();

        this.getTestDataFromFile();
    }

    private void getBrowserName() {
         this.browserName = ReadProperties.readPropertiesFile("browser");
    }

    protected ArrayList<String> getBrowserTabs() {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        return tabs;
    }

    protected void getTestDataFromFile() {
        Properties prop = new Properties();
        try {
            String path = System.getProperty("user.dir");
            String relativePath = "\\src\\main\\java\\config\\testsdata.properties";
            String fullPath = path + relativePath;
            InputStream input = new FileInputStream(fullPath);
            prop.load(input);
            this.success_email = prop.getProperty("success_email");
            this.setUpdateTestDataToFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setUpdateTestDataToFile() {
        Properties prop = new Properties();
        try {
            String path = System.getProperty("user.dir");
            String relativePath = "\\src\\main\\java\\config\\testsdata.properties";
            String fullPath = path + relativePath;
            OutputStream output = new FileOutputStream(fullPath);
            String partName = this.success_email.substring(0,10);
            int i = Integer.parseInt(partName);
            i += 1;
            String randomStirng = RandomStringUtils.randomAlphabetic(12);
            String newTestCaseName = i + randomStirng + "@yopmail.com";
            prop.setProperty("success_email", newTestCaseName);
            prop.store(output, "Test data updated: success test case");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setBrowser(String browser, String port) throws MalformedURLException, NullPointerException {
        /* CHROME */
        if (this.browserName.equalsIgnoreCase("chrome")) {
            String chromeLocation = System.getProperty("user.dir") + ReadProperties.readPropertiesFile("chromeDriverLocation").toString();
            System.setProperty("webdriver.chrome.driver", chromeLocation);
            this.driver = new ChromeDriver();

        /* FIREFOX */
        } else if (this.browserName.equalsIgnoreCase("firefox")) {
            String geckoLocation = System.getProperty("user.dir") + ReadProperties.readPropertiesFile("geckoDriverLocation").toString();
            System.setProperty("webdriver.gecko.driver", geckoLocation);
            this.driver = new FirefoxDriver();

        /* EDGE */
        } else if (this.browserName.equalsIgnoreCase("edge")){
            this.driver = new EdgeDriver();

        /* REMOTE CHROME DOCKER */
        } else if (this.browserName.equalsIgnoreCase("remotechrome")) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browserName", "chrome");
            caps.setCapability("platform", Platform.LINUX);
            caps.setCapability("version", "");
            this.driver = new RemoteWebDriver(new URL("http://0.0.0.0:4444/wd/hub"), caps);

        /* REMOTE CHROME DOCKER */
        } else if (this.browserName.equalsIgnoreCase("remotefirefox")) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browserName", "firefox");
            caps.setCapability("platform", Platform.LINUX);
            caps.setCapability("version", "");
            this.driver = new RemoteWebDriver(new URL("http://0.0.0.0:4445/wd/hub"), caps);

        /* GRID WITH DOCKER */
        } else if (this.browserName.equalsIgnoreCase("grid")) {
            /* Chrome */
            if (port.equalsIgnoreCase("5900") && browser.equalsIgnoreCase("chrome")) {
                String nodeURL = "http://0.0.0.0:4444/wd/hub";
                DesiredCapabilities caps = DesiredCapabilities.chrome();
                caps.setPlatform(Platform.LINUX);
                caps.setVersion("");
                this.driver = new RemoteWebDriver(new URL(nodeURL), caps);
            }
            /* Firefox */
            else if (port.equalsIgnoreCase("5901") && browser.equalsIgnoreCase("firefox")) {
                String nodeURL = "http://0.0.0.0:4444/wd/hub";
                DesiredCapabilities caps = DesiredCapabilities.firefox();
                caps.setPlatform(Platform.LINUX);
                caps.setVersion("");
                this.driver = new RemoteWebDriver(new URL(nodeURL), caps);
            }
        } else {
            System.out.println("======= NO DRIVER WAS SELECTED =======");
        }
    }
}
