package operations;

import org.openqa.selenium.WebDriver;
import pages.ForgotPasswordPage;
import pages.HomePage;
import pages.LoginPage;
import pages.SignUpPage;

import java.util.ArrayList;

public class Actions {

    private ArrayList<String> tabs;
    private WebDriver driver;

    public Actions(WebDriver driver) {
        this.driver = driver;
    }

    public void signUp(String email) {

        HomePage homePage = new HomePage(driver);
        homePage.clickLogin();

        tabs = getTabs(driver);
        //System.out.println(tabs.toString());
        if (tabs.size() > 1) {
            driver.switchTo().window(tabs.get(1));
        }

        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickForgotPassword();

        ForgotPasswordPage forgotPasswordPage = new ForgotPasswordPage(driver);
        forgotPasswordPage.clickRegistrarse();

        SignUpPage signUpPage = new SignUpPage(driver);
        signUpPage.submitEmail(email);
    }

    private ArrayList<String> getTabs(WebDriver driver) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        return tabs;
    }
}
