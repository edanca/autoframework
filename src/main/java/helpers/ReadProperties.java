package helpers;

import base.BasePage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {

    static Properties prop = new Properties();

    public static void main(String[] args) {
        readPropertiesFile("browser");
        //readTestDataFile();
    }

    public static String readPropertiesFile(String propName) {

        try {
            //InputStream input = new FileInputStream("C:\\Users\\EDU-PC\\Documents\\Proyectos\\Java\\Testing\\autoframework\\src\\main\\java\\config\\config.properties");
            String path = System.getProperty("user.dir");
            String relativePath = "\\src\\main\\java\\config\\config.properties";
            String fullPath = path + relativePath;
            InputStream input = new FileInputStream(fullPath);
            prop.load(input);
            return prop.getProperty(propName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void readTestDataFile() {

        try {
            String path = System.getProperty("user.dir");
            String relativePath = "\\src\\main\\java\\config\\testsdata.properties";
            String fullPath = path + relativePath;
            InputStream input = new FileInputStream(fullPath);
            prop.load(input);
            String data = prop.getProperty("success_email");
            System.out.println(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
