package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class HomePage extends BasePage {

    private WebDriver driver;

    @FindBy(css = "#container > header > nav > ul:nth-child(4) > li > a")
    private WebElement btnLogin_elem;
    @FindBy(css = "#container > header > nav > ul.c-nav__list.c-nav__list--end > li:nth-child(4) > a")
    private WebElement linkSearch_elem;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, this.pageExplicitWaitFromFile()), this);
    }

    public void clickLogin() {
        btnLogin_elem.click();
    }

    public void clickSearch() {

    }
}
