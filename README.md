# Auto Framework

## Test cases
SignUpShould
- success
- fail By Already Registered Email
- fail By Incorrect Email
- fail By Empty Email

## Pre-requisites
- Install Java 1.8 runtime and set it in Windows Enviroment Path.
- Install Maven - [Video Tutorial](https://www.youtube.com/watch?v=1bDd5B8TA2g).
- Install Git.
- No IDE is needed to execute the test.

## Steps to run tests
- Clone the framework from [HERE](https://gitlab.com/edanca/autoframework).
- Once is downloaded, use command line or any other command line tool and locate to the clone directory.
- Run the following command:
    > mvn test
- After the build is completed you should see "BUILD SUCCESS".

## Notes
To change which selenium driver should be used must change the value for "browser" at config.propreties.

### Possible values:

+ *Chrome* -> Chromedriver
+ *Firefox* -> Geckodriver
+ *Edge* -> Edgedriver
+ *RemoteChrome* -> Standalone Docker container with Chrome.
    - Need to have Docker container up and running at port 5900.
+ *RemoteFirefox* -> Standalone Docker container with Firefox.
    - Need to have Docker container up and running at port 5901.
+ *Grid* -> Using the Selenium Grid docker container up and runnnig run test on its nodes.
    - Need to configure Docker selenium hub and Chrome and Firefox nodes.
    - Need to execute from grid.xml instead of testng.xml.
