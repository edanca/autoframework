package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class LoginPage extends BasePage {

    private WebDriver driver;

    //@FindBy(css = "fieldset.adb-container > div.adb-container_content > div.adb-form--field > div > a")
    @FindBy(xpath = "//a[@class='forgotPassword']")
    private WebElement linkForgotPassword_elem;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, this.pageExplicitWaitFromFile()), this);
    }

    public void clickForgotPassword() {
        linkForgotPassword_elem.click();
    }
}
